#!/bin/bash
#-------------------------------------------------------------------------------
#  \author Jan Piotr Buchmann <jpb@members.fsf.org>
#  \copyright 2020
# Usage: pdfreduce.sh infile.pdf [outfile.pdf] [pdfsetting]
#
# pdfsettings:
# screen   (screen-view-only quality, 72 dpi images)
# ebook    (low quality, 150 dpi images)
# printer  (high quality, 300 dpi images)
# prepress (high quality, color preserving, 300 dpi imgs)
# default  (almost identical to /screen)
#-------------------------------------------------------------------------------


set -o errexit -o pipefail -o noclobber -o nounset

function filebasename()
{
  echo -n "${1%.pdf}"
}

function mkbkp()
{
  local bkp="${1}.bak"
  cp "$1" "${bkp}"
  echo -n "${bkp}"
}

pdfsetting=${3:-printer}
outfile="${2:-$(filebasename $1).reduced.pdf}"
printf "Input: %s\nOutput: %s\nBackup:%s\n" "$1" "${outfile}" $(mkbkp $1)
gs -sDEVICE=pdfwrite            \
   -dCompatibilityLevel=1.4     \
   -dNOPAUSE                    \
   -dQUIET                      \
   -dBATCH                      \
   -dSAFER                      \
   -dPDFSETTINGS=/${pdfsetting} \
   -sOutputFile=${outfile} "$1"
